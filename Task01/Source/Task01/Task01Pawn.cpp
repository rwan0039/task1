// Fill out your copyright notice in the Description page of Project Settings.


#include "Task01Pawn.h"
#include "ShootableActor.h"


// Sets default values
ATask01Pawn::ATask01Pawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AutoPossessPlayer = EAutoReceiveInput::Player0;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	mCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	mCamera->SetupAttachment(RootComponent);

}

void ATask01Pawn::MoveForward(float axisValue)
{
	mMovementInput.X = axisValue;
}

void ATask01Pawn::MoveRight(float axisValue)
{
	mMovementInput.Y = axisValue;
}

void ATask01Pawn::Shoot()
{
	// Print a message to the output log
	UE_LOG(LogTemp, Warning, TEXT("Bang"));
	// Set up a trace to see if we can hit something
	FHitResult linetraceResult;
	// Stat position of check
	FVector startTrace = GetActorLocation();
	startTrace.X += 200;
	// End position of check
	FVector endTrace = (GetActorForwardVector() * 7500.0f) + startTrace;
	FCollisionQueryParams params;
	// Attempt to check
	bool isHit = GetWorld()->LineTraceSingleByChannel(linetraceResult, startTrace, endTrace,
		ECC_WorldStatic, params);
	// If something was hit
	if (isHit) {
		// Attempt to cast to our shootable object
		AShootableActor* shootActor = Cast<AShootableActor>(linetraceResult.GetActor());
		if (shootActor) {
			// If successful case then display hit message and call OnBulletHit
			UE_LOG(LogTemp, Warning, TEXT("Hit Shoot Target"));
			shootActor->OnBulletHit();
		}
	}
}

// Called when the game starts or when spawned
void ATask01Pawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATask01Pawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!mMovementInput.IsZero()) {
		mMovementInput.Normalize();
		SetActorLocation(GetActorLocation() + (mMovementInput * speed * DeltaTime));
	}
}

// Called to bind functionality to input
void ATask01Pawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &ATask01Pawn::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATask01Pawn::MoveRight);
	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this,
		&ATask01Pawn::Shoot);

}

