// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Task01GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TASK01_API ATask01GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
