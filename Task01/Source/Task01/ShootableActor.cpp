// Fill out your copyright notice in the Description page of Project Settings.


#include "ShootableActor.h"

// Sets default values
AShootableActor::AShootableActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	mVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	mVisibleComponent->SetupAttachment(RootComponent);
	Speed = 1;
	JumpTime = 30;
}


// Called when the game starts or when spawned
void AShootableActor::BeginPlay()
{
	Super::BeginPlay();
	RandomlyLeftRightTime();
}

// Called every frame
void AShootableActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Time = Time + 1;

	if (Time >= 0 && Time < JumpTime)
	{
		FVector DeltaLocation = FVector(0, 0, 2 * Speed);
		AddActorLocalOffset(DeltaLocation);
	}
	if (Time >= JumpTime && Time < (2 * JumpTime))
	{
		FVector DeltaLocation = FVector(0, 0, -2 * Speed);
		AddActorLocalOffset(DeltaLocation);
	}
	if (Time > (2 * JumpTime) && Time < LeftTime)
	{
		FVector DeltaLocation = FVector(1, 0, 0);
		AddActorLocalOffset(DeltaLocation);
	}
	if (Time > LeftTime && Time < RightTime)
	{
		FVector DeltaLocation = FVector(-1, 0, 0);
		AddActorLocalOffset(DeltaLocation);
	}

	if (Time > RightTime)
	{
		Time = 0;
	}
}

void AShootableActor::OnBulletHit()
{
	Destroy();
}

void AShootableActor::RandomlyLeftRightTime()
{
	LeftTime = FMath::RandRange(300.f, 350.f);
	RightTime = FMath::RandRange(LeftTime, 450.f);
}

