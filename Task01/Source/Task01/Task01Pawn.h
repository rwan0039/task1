// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Camera/CameraComponent.h"

#include "Task01Pawn.generated.h"

UCLASS()
class TASK01_API ATask01Pawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATask01Pawn();

protected:
	// Called when the game starts or when spawned

	void MoveForward(float axisValue);
	void MoveRight(float axisValue);
	void Shoot();
	float speed = 500.0f;
	FVector mMovementInput;
	UCameraComponent* mCamera;


	virtual void BeginPlay() override;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
