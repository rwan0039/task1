// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ShootableActor.generated.h"

UCLASS()
class TASK01_API AShootableActor : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
		float Speed;

	float Time;
	float JumpTime;
	float LeftTime;
	float RightTime;

public:
	// Sets default values for this actor's properties
	AShootableActor();
	void OnBulletHit();
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* mVisibleComponent;

	void  RandomlyLeftRightTime();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
